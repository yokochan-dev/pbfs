# pbfs
## Powerful Bash file searcher

## Introduction
pbfs is a CLI tool which performs powerful searches in a directory tree. It is able to search files containing a pattern (both basic and extended regexp), between a maximum and minimum size, of specific extensions.<br>
In the future other types of searches will be added to make the tool even more powerful.

## Usage
`./pbfs.bash -h` displays a useful help.<br>

Usage: `./pbfs.bash -d|--directory <path/to/directory/> -p|--pattern <pattern>`  
Optional flags:  

* `-i` to ignore case in pattern     
* `-e` to use extended regexp    
* `-t <type1 type2 type3 ...>` to only search selected file types separated by space and in quotes. Ex. 'json conf txt'    
* `-gt <size in bytes>` to search only files of size > than the parameter.  
* `-lt <size in bytes>` to search only files of size < than the parameter.  
* `-u|--user <user>` to search only files of specified owner.  
* `-ca <year/month/day>` to search only files created AFTER the specified date.  
* `-cb <year/month/day>` to search only files created BEFORE the specified date.  
* `-g|--group <group>` to search only files of specified user.

## License
Please refer to the LICENSE file.
