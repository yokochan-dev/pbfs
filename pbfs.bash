#!/bin/bash

findReadableFiles(){

	directory="$1"
	elements=("$directory"*)
	for element in "${elements[@]}"; do
		if [ "$element" = "$directory" ]; then
			continue
		elif [ -d "$element" ]; then
			directoriesToScan[${#directoriesToScan[@]}]="$element/"
		elif [ -r "$element" ]; then
			filesToCheck[${#filesToCheck[@]}]="$element"
		fi
	done

}

checkExtension(){

	if [ ${#fileTypes[@]} -gt 0 ]; then
		#Check file extension
		local filename=$(basename "$1")
		local extension="${filename##*.}"
		#Cycle through the extension and check if the file matches
		for el in "${fileTypes[@]}"; do
			if [ "$el" = "$extension" ]; then
				return 1
			fi
		done
	fi
	return 0

}

checkSizeGreater(){

	if [ $1 -gt $greaterThan ]; then
		return 1
	fi
	return 0

}

checkUser(){

	if [ "$1" = "$userParam" ]; then
		return 1
	fi
	return 0

}

checkSizeLess(){

	if [ $1 -lt $lessThan ]; then
		return 1
	fi
	return 0

}

checkCreationDateAfter(){

	if [ "$creationDateEpoch" -eq 0 ]; then
		return 0
	fi

	if [ $1 -gt "$creationDateAfterParamEpoch" ]; then
		return 1
	fi
	return 0

}

checkCreationDateBefore(){
	
	if [ "$creationDateEpoch" -eq 0 ]; then
		return 0
	fi

	if [ $1 -lt "$creationDateBeforeParamEpoch" ]; then
		return 1
	fi
	return 0
}

checkGroup(){

	if [ "$1" = "$groupParam" ]; then
		return 1
	fi
	return 0

}

approveFile(){
	
	#All flags are initialized at 1 so even if control isn't requested it still passes
	local let validExtension=1
	local let validGreater=1
	local let validLess=1
	local let validUser=1
	local let validCreationDateAfter=1
	local let validGroup=1
	local let validCreationDateBefore=1
	local let filesize=$(stat -c %s "$1")
	local user=$(stat -c %U "$1")
	local group=$(stat -c %G "$1")
	local let creationDateEpoch=$(stat -c %W "$1")
	#If extension check is requested perform
	if [ ${#fileTypes[@]} -gt 0 ]; then
		checkExtension $1
		validExtension=$?
	fi
	#If dimension greater is requested perform
	if [ "$checkGt" = "true" ]; then
		checkSizeGreater  $filesize
		validGreater=$?
	fi
	#If dimension less is requested perform
	if [ "$checkLt" = "true" ]; then 
		checkSizeLess $filesize
		validLess=$?
	fi
	#If user check is requested perform
	if [ "$checkU" = "true" ]; then
		checkUser "$user"
		validUser=$?
	fi
	#If creation date after is requested perform
	if [ "$checkCa" = "true" ]; then
		checkCreationDateAfter "$creationDateEpoch"
		validCreationDateAfter=$?
	fi
	#If creation date before is requested perform
	if [ "$checkCb" = "true" ]; then
		checkCreationDateBefore "$creationDateEpoch"
		validCreationDateBefore=$?
	fi
	#If group check is requested perform
	if [ "$checkG" = "true" ]; then
		checkGroup "$group"
		validGroup=$?
	fi
	#Final control
	if [ -r "$1" -a "$validExtension" -eq 1 -a "$validGreater" -eq 1 -a "$validLess" -eq 1 -a "$validUser" -eq 1 \
		-a "$validCreationDateAfter" -eq 1 -a "$validCreationDateBefore" -eq 1 -a "$validGroup" -eq 1 ]; then

		local res=$(cat "$1" | grep $flagInsensitivegrep $flagExtendedgrep $flagIgnoreBinarygrep "$pattern")
		echo "$1"
		if [ "$res" != "" ]; then
			filesApproved[${#filesApproved[@]}]="$1"
		fi
	fi

}

showHelp(){

	echo "Usage: ./bffc.bash -d|--directory <path/to/directory/> -p|--pattern <pattern>"
	echo "Optional flags:"
	echo "-i|--insensitive to ignore case in pattern"
	echo "-e|--extended to use extended regexp"
	echo "-t|--type <type1 type2 type3 ...> to only search selected file types separated by space and in quotes. Ex. 'json conf txt'" 
	echo "-gt <size in bytes> to search only files of size > than the parameter."
	echo "-lt <size in bytes> to search only files of size < than the parameter."
	echo "-u|--user <user> to search only files of specified user."
	echo "-ca <year/month/day> to search files created AFTER the specified time. If the creation date of a file can't be retrieved said file won't be counted."
	echo "-cb <year/month/day> to search files created BEFORE the specified time. If the creation date of a file can't be retrieved said file won't be counted."
	echo "-g|--group <group> to search only files of specified group."
}

directoriesToScan=()
filesToCheck=()
filesApproved=()
flagInsensitivegrep=''
flagExtendedgrep=''
flagIgnoreBinarygrep='-I'
pattern=''
fileTypes=()
checkGt="false"
checkLt="false"
checkU="false"
checkCa="false"
checkCb="false"
checkG="false"

let i=0
let nparameters=$#

while [ $nparameters -gt "$i" ]; do
	case $1 in
		-h|--help)
			showHelp
			exit
			;;
		-d|--directory)
			directoriesToScan[${#directoriesToScan[@]}]="$2"
			shift
			shift
			let i=i+2
			;;
		-i|--insensitive)
			flagInsensitivegrep='-i'
			shift
			let i=i+1
			;;
		-e|--extended)
			flagExtendedgrep='-e'
			let i=i+1
			shift
			;;
		-p|--pattern)
			pattern="$2"
			shift
			shift
			let i=i+2
			;;
		-t|--type)
			IFS=' '
			read -ra fileTypes <<< "$2"
			shift
			shift
			let i=i+2
			;;
		-gt)
			checkGt="true"
			let greaterThan=$2
			shift
			shift
			let i=i+2
			;;
		-lt)
			checkLt="true"
			let lessThan=$2
			shift
			shift
			let i=i+2
			;;
		-u|--user)
			userParam="$2"
			checkU="true"
			shift
			shift
			let i=i+2
			;;
		-ca)
			creationDateAfterParam="$2"
			let creationDateAfterParamEpoch=$(date -d "$creationDateAfterParam" +"%s")
			if [ $? -ne 0 ]; then
				echo "Date inserted not valid."
				exit
			fi
			checkCa="true"
			shift
			shift
			let i=i+2
			;;
		-cb)
			creationDateBeforeParam="$2"
			let creationDateBeforeParamEpoch=$(date -d "$creationDateBeforeParam" +"%s")
			if [ $? -ne 0 ]; then
				echo "Date inserted not valid."
				exit
			fi
			checkCb="false"
			shift
			shift
			let i=i+2
			;;
		-g|--group)
			groupParam="$2"
			checkG="true"
			shift
			shift
			let i=i+2
			;;
		*)
			echo "Parameter not found."
			exit
	esac
done

let i=0
while [ $i -ne "${#directoriesToScan[@]}" ]; do
	findReadableFiles "${directoriesToScan[i]}"
	let i=i+1
done
#Cycle through the files to check array until it's empty
let i=0
while [ $i -ne "${#filesToCheck[@]}" ]; do
	approveFile "${filesToCheck[i]}" "$pattern"
	let i=i+1
done
#Print the correct files
for file in "${filesApproved[@]}"; do
	echo "$file"
done

